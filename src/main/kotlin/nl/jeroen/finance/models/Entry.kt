package nl.jeroen.finance.models

import java.util.*
import javax.persistence.*

@Entity
class Entry(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,

        @Column(nullable = false)
        var date: Date? = null,

        @Column(nullable = false)
        var amount: Double? = null,

        @Column(nullable = false, columnDefinition = "longtext")
        var description: String? = null,

        @ManyToOne
        var store: Store? = null
)
