package nl.jeroen.finance.models

import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
class Store(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,

        @field:NotBlank
        @Column(nullable = false)
        var name: String? = null,

        @field:NotBlank
        @Column(nullable = false)
        var matcher: String? = null,

        @OneToMany(targetEntity = Entry::class, mappedBy = "store")
        val entries: MutableList<Entry>? = null
) {
    @PreRemove
    fun preRemove() {
        if (this.entries == null) return
        for (entry in this.entries) {
            entry.store = null
        }
    }
}