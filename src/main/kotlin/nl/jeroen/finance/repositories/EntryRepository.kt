package nl.jeroen.finance.repositories

import nl.jeroen.finance.models.Entry
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface EntryRepository : CrudRepository<Entry, Long>