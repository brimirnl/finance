package nl.jeroen.finance.repositories

import nl.jeroen.finance.models.Store
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface StoreRepository : CrudRepository<Store, Long>