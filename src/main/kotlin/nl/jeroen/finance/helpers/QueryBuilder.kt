package nl.jeroen.finance.helpers

import nl.jeroen.finance.models.Store
import javax.persistence.EntityManager
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root
import kotlin.reflect.KClass

class QueryBuilder<T : Any>(private val entityManager: EntityManager, model: KClass<T>) {
    private val builder: CriteriaBuilder = this.entityManager.criteriaBuilder
    private val query: CriteriaQuery<T>
    private val root: Root<T>
    private var predicate: Predicate

    enum class Operator {
        LIKE, EQUAL
    }

    enum class Order {
        ASC, DESC
    }

    init {
        this.query = this.builder.createQuery(model.java)
        this.root = this.query.from(model.java)
        this.predicate = this.builder.and()
    }

    fun getResults(): MutableList<T> {
        this.query.where(this.predicate)
        return this.entityManager.createQuery(this.query)
                .resultList!!
    }

    fun <M> orderBy(field: String, order: Order = Order.ASC) {
        this.query.orderBy(when (order) {
            Order.ASC -> this.builder.asc(this.root.get<M>(field))
            Order.DESC -> this.builder.desc(this.root.get<M>(field))
        })
    }

    fun <M> where(field: String, value: String, operator: Operator = Operator.EQUAL) {
        this.predicate = this.builder.and(this.predicate, when (operator) {
            Operator.EQUAL -> this.builder.equal(this.root.get<M>(field), value)
            Operator.LIKE -> this.builder.like(this.root.get(field), value)
        })
    }

    fun <M : Any> whereExists(model: KClass<M>, fieldChild:String, fieldParent:String) {
        val subQuery = this.query.subquery(model.java)
        val root = subQuery.from(model.java)
        subQuery.select(root)
        subQuery.where(
                this.builder.equal(
                        root.get<M>(fieldChild),
                        this.root.get<Any>(fieldParent)
                )
        )

        this.predicate = this.builder.and(this.predicate, this.builder.exists(subQuery))
    }
}