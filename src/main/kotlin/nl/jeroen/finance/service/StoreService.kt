package nl.jeroen.finance.service

import nl.jeroen.finance.helpers.QueryBuilder
import nl.jeroen.finance.models.Entry
import nl.jeroen.finance.models.Store
import nl.jeroen.finance.repositories.StoreRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.logging.Logger

@Service
class StoreService(
        @Autowired private val entryService: EntryService,
        @Autowired repository: StoreRepository
) : EntityService<Store, Long>(repository) {

    @Suppress("FINAL_UPPER_BOUND")
    override fun <S : Store> save(s: S): S {
        super.save(s)
        Logger.getLogger(this::class.qualifiedName).info("testing")
        this.entryService.setStoreWhichMatch(s)

        return s
    }

    fun getStatistics(): MutableList<Store> {
        val results = QueryBuilder(this.entityManager, Store::class)
                .also { it.whereExists(Entry::class, "store", "id") }
                .getResults()

        for (result in results) {
            println(result.name)
            val stats = result.entries!!
                    .stream()
                    .mapToDouble { it.amount ?: 0.0 }
                    .summaryStatistics()

            println(stats.min)
            println(stats.max)
            println(stats.average)
        }

        return results
    }
}