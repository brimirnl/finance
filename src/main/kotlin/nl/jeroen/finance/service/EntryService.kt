package nl.jeroen.finance.service

import nl.jeroen.finance.helpers.QueryBuilder
import nl.jeroen.finance.models.Entry
import nl.jeroen.finance.models.Store
import nl.jeroen.finance.repositories.EntryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class EntryService(@Autowired repository: EntryRepository) : EntityService<Entry, Long>(repository) {
    enum class OrderField(
            val orderName: String
    ) {
        DATE("date")
    }

    fun listOrderedBy(order: OrderField): MutableList<Entry>? {
        return QueryBuilder(this.entityManager, Entry::class)
                .also { it.orderBy<String>(order.orderName, QueryBuilder.Order.DESC) }
                .getResults()
    }

    fun setStoreWhichMatch(store: Store): MutableList<Entry> {
        // TODO: Create update to update this all at once without first fetching them
        val items = QueryBuilder(this.entityManager, Entry::class)
                .also { it.where<String>("description", "%${store.matcher}%", QueryBuilder.Operator.LIKE) }
                .getResults()
        for (item in items) {
            item.store = store
        }

        this.saveAll(items)

        return items
    }
}