package nl.jeroen.finance.service

import nl.jeroen.finance.models.Store
import org.springframework.data.repository.CrudRepository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

abstract class EntityService<T, ID>(private val repository: CrudRepository<T, ID>) {

    @PersistenceContext
    protected lateinit var entityManager: EntityManager

    open fun <S : T> save(s: S): S {
        return this.repository.save(s)
    }

    open fun <S : T> saveAll(iterable: Iterable<S>): Iterable<S> {
        return this.repository.saveAll(iterable)
    }

    open fun findById(id: ID): java.util.Optional<T> {
        return this.repository.findById(id)
    }

    open fun existsById(id: ID): Boolean {
        return this.repository.existsById(id)
    }

    open fun findAll(): Iterable<T> {
        return this.repository.findAll()
    }

    open fun findAllById(iterable: Iterable<ID>): Iterable<T> {
        return this.repository.findAllById(iterable)
    }

    open fun count(): Long {
        return this.repository.count()
    }

    open fun deleteById(id: ID) {
        return this.repository.deleteById(id)
    }

    open fun delete(t: T) {
        return this.repository.delete(t)
    }

    open fun deleteAll(iterable: Iterable<T>) {
        return this.repository.deleteAll(iterable)
    }

    open fun deleteAll() {
        return this.repository.deleteAll()
    }

}