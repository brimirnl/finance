package nl.jeroen.finance.controllers

import nl.jeroen.finance.service.EntityService
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.util.logging.Logger
import javax.validation.Valid

abstract class CrudController<T, ID>(
        private val route: String,
        private val viewName: String,
        private val service: EntityService<T, ID>
) {

    private val LOGGER = Logger.getLogger(CrudController::class.qualifiedName)

    @GetMapping("")
    fun index(): ModelAndView {
        return ModelAndView("$viewName/index")
                .addObject("items", this.service.findAll())
    }

    @GetMapping("/add")
    fun add(@ModelAttribute("item") item: T): ModelAndView {
        return ModelAndView("$viewName/add")
                .addObject("item", item)
    }

    @PostMapping("/add")
    fun store(redirectAttributes: RedirectAttributes, @ModelAttribute("item") @Valid item: T, result: BindingResult): String {
        if (result.hasErrors()) {
            return "$viewName/add"
        }

        redirectAttributes.addFlashAttribute("success", "Item is toegevoegd")
        this.service.save(item)

        this.LOGGER.info("Redirecting to $route")
        return "redirect:$route"
    }

    @GetMapping("/edit/{item}")
    fun edit(@ModelAttribute("item") item: T?): ModelAndView {

        return ModelAndView("$viewName/edit")
                .addObject("item", item)
    }

    @PostMapping("/edit/{item}")
    fun update(redirectAttributes: RedirectAttributes, @ModelAttribute("item") @Valid item: T, result: BindingResult): String {
        if (result.hasErrors()) {
            return "$viewName/edit"
        }

        this.service.save(item)
        redirectAttributes.addFlashAttribute("success", "Item is geupdate")

        return "redirect:$route"
    }

    @GetMapping("/delete/{item}")
    fun delete(redirectAttributes: RedirectAttributes, @ModelAttribute("item") item: T): String {
        this.service.delete(item)
        redirectAttributes.addFlashAttribute("success", "Item is verwijderd")
        return "redirect:$route"
    }
}