package nl.jeroen.finance.controllers

import nl.jeroen.finance.models.Store
import nl.jeroen.finance.service.StoreService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/store")
class StoreController(@Autowired service: StoreService) :
        CrudController<Store, Long>("/store", "store", service)