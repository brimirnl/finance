package nl.jeroen.finance.controllers

import com.opencsv.CSVParserBuilder
import com.opencsv.CSVReaderBuilder
import nl.jeroen.finance.models.Entry
import nl.jeroen.finance.service.EntryService
import nl.jeroen.finance.service.StoreService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.ModelAndView
import java.io.StringReader
import java.text.SimpleDateFormat
import java.util.logging.Logger

@Controller
class UploadController {

    private val LOGGER = Logger.getLogger(UploadController::class.qualifiedName)

    @Autowired
    private lateinit var storeService: StoreService
    @Autowired
    private lateinit var entryService: EntryService

    @GetMapping("/form")
    fun form(): ModelAndView {
        return ModelAndView("form")
    }

    @GetMapping("/")
    fun index(): ModelAndView {
        return ModelAndView("index")
                .addObject("stats", this.storeService.getStatistics())
                .addObject("items", this.entryService.listOrderedBy(EntryService.OrderField.DATE))
    }

    @PostMapping("/form")
    fun handleForm(@RequestParam("file") file: MultipartFile): String {
        val reader = CSVReaderBuilder(StringReader(String(file.bytes)))
                .withCSVParser(CSVParserBuilder()
                        .withSeparator('\t')
                        .build())
                .build()
        var line = reader.readNext()
        while (line != null) {
            val entry = Entry(
                    date = SimpleDateFormat("yyyyMMdd").parse(line[2]),
                    amount = line[6].replace(',', '.').toDouble(),
                    description = line[7]
            )
            this.entryService.save(entry)
            this.LOGGER.info("Line ${entry.id} has been imported")
            line = reader.readNext()
        }

        this.LOGGER.info("Everything has been imported.")

        return "redirect:/"
    }
}